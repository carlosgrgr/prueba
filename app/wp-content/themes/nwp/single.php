<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package nwp_theme
 */
get_header(); ?>
    <?php if ( have_posts() ) : 
        while ( have_posts() ) : the_post();
            switch ( $post->post_type ) {
                case 'post':
                default :
                    get_template_part( 'src/Posts/views/content', $post->post_type );
                    break;
            }
        endwhile; 
    endif;
get_footer();
