<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nwp_theme
 */

?><!DOCTYPE html>
<html <?php echo language_attributes(); ?>>
  	<head>
		<meta charset="UTF-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>  
		<?php get_template_part( 'template-parts/nav/content-header', 'nav' ); ?>
		<main role="main">

		