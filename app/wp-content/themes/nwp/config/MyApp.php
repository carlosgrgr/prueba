<?php
namespace App;

if ( ! defined( 'ABSPATH' ) ) { exit; }

use Nwp\Security\NwpSecurity;
use Nwp\Setup\NwpSetup;
use Nwp\Setup\NwpMedia;
use Nwp\Dashboard\NwpDashboard;
use Nwp\Assets\NwpAssets;
use Nwp\WPO\NwpWPO;
use Nwp\TinyMcePlugins\NwpTinyMcePlugins;
use Nwp\MultilingualPress\NwpMultilingualPress;

class MyApp
{
    public $app_name;
    public $app_version;
    public $app_css_version;
    public $app_js_version;

    /**
     *
     */
    public function __construct()
    {
        $this->define_constants();
        $this->app_name         = defined( 'APP_NAME' ) ? APP_NAME : 'nwp';
        $this->app_version      = defined( 'APP_VERSION' ) ? APP_VERSION : '1.0';
        $this->app_css_version  = defined( 'APP_CSS_VERSION' ) ? APP_CSS_VERSION : '1.0';
        $this->app_js_version   = defined( 'APP_JS_VERSION' ) ? APP_JS_VERSION : '1.0';
    }

    /**
     * define_constants function
     *
     * @return void
     */
    private function define_constants()
    {
        $this->define( 'APP_NAME', 'nateevo' );
        $this->define( 'APP_VERSION', '1.0' );
        $this->define( 'APP_CSS_VERSION', '1.0' );
        $this->define( 'APP_JS_VERSION', '1.0' );
        $this->define( 'APP_TEXTDOMAIN', 'nateevo' );
        $this->define( 'APP_PATH', get_template_directory_uri() . '/' );
        $this->define( 'ASSETS_PUBLIC_PATH', get_template_directory_uri() . '/public/assets/' );
    }

    /**
     * define function
     *
     * @param [string] $name
     * @param [string] $value
     *
     * @return void
     */
    private function define( $name, $value )
    {
        if ( ! defined( $name ) ) {
            define( $name, $value );
        }
    }

    /**
     * init function
     *
     * @return void
     */
    public function init()
    {
        $this->init_vendor_classes();
        $this->init_src_classes();
    }

    /**
     * init function
     *
     * init all components hooks
     *
     * @return void
     */
    private function init_vendor_classes()
    {
        $this->security_init();
        $this->setup_init();
        $this->dashboard_init();
        $this->customizer_init();
        $this->public_assets_init();
        $this->wpo_init();
        $this->tinymce_plugins_init();
        $this->multilingual_press_init();
    }

    private function init_src_classes()
    {
        $this->helpers();
        $this->navwalker_init();
        $this->posts_init();
        $this->pages_init();
        $this->front_page_init();
    }

    /**
     * security_init function
     *
     * $api_rest, $app_recaptcha, $app_login_slug
     * Set [bool] $api_rest to true for enable API REST
     * Set [array] $app_recaptcha values for enable reCaptcha [bool] enable, [string] site_key, [string] private_key, [string] version (invisible or V2) - only available in PRE and PRO ENVIRONMENT
     * Ser [string] $app_login_slug for more security and hide default paths wp-login.php and wp-admin
     * Set [string] $cap_access_dashboard for restrict access to dashboard and hide admin_bar
     * reCaptcha use action google_recaptcha_response and use shortcode google_recaptha for display it
     *
     * @return void
     */
    private function security_init()
    {
        $app_recaptcha = array(
            'enable'        => false,
            'site_key'      => '6LcJfLsUAAAAAOtqcSDJCG-iOEeHDalvO5i7_-Mc',
            'private_key'   => '6LcJfLsUAAAAAOQGze8qdf3qi_oIdsCHLPPq3Z3M',
            'version'       => 'V3',
        );
        $app_security = new NwpSecurity;
        $app_security->set_api_rest( $api_rest = true );
        $app_security->set_login_slug( $app_login_slug = '' );
        $app_security->set_recaptcha( $app_recaptcha );
        $app_security->set_roles_access_dashboard( $roles = array( 'administrator', 'editor', 'author' ) );
        add_action( 'after_setup_theme', array( $app_security, 'init' ) );
    }

    /**
     * setup_init function
     *
     * Set [object] Media settings method from Nwp_App_Media class
     * Set [bool] $app_comments_disable to true for disable comments
     *
     * @return void
     */
    private function setup_init()
    {
        $app_media = $this->media_settings();
        $app_setup = new NwpSetup( $app_media );
        $app_setup->set_menus( $app_menus = array(
            'top-menu'          => __( 'Top Menu', APP_TEXTDOMAIN ),
            'main-menu'         => __( 'Main Menu', APP_TEXTDOMAIN ),
            'country-menu'      => __( 'Country Menu', APP_TEXTDOMAIN ),
            'footer-menu'       => __( 'Footer Menu',  APP_TEXTDOMAIN ),
            'footer-legal-menu' => __( 'Footer Legal Menu',  APP_TEXTDOMAIN ),
        ) );
        $app_setup->set_comments( $app_comments_disable = true );
        $app_setup->set_gutenberg_editor( $app_gutenberg_editor = true );
        $app_registered_blocks = array(
            'core/gallery',
            'core/image',
            'core/paragraph',
            'core/heading',
            'core/quote',
            'core/embed',
            'core/list',
            'core/button',
            'core/freeform',
            'core/block',
            //'core/table',
            'acf/advantages-boxes',
            'acf/pages-list',
            'acf/featured-information-pagination',
            'acf/contact',
            'acf/header-pages',
            'acf/featured-information',
            'acf/information',
            'acf/titles-pages',
            'acf/testimonials',
            'acf/custom-table',
            'acf/banner',
            'acf/baner-boxes',
        );
        $app_setup->set_allowed_gutenberg_blocks( $app_registered_blocks );
        $unregister_default_widgets = array(
            'WP_Widget_Calendar',
            'WP_Widget_Pages',
            'WP_Widget_Archives',
            'WP_Widget_Links',
            'WP_Widget_Meta',
            'WP_Widget_Search',
            'WP_Widget_Text',
            'WP_Widget_Recent_Comments',
            'WP_Widget_RSS',
            'WP_Nav_Menu_Widget',
            'WP_Widget_Media_Image',
            'WP_Widget_Media_Audio',
            'WP_Widget_Media_Video',
            'WP_Widget_Media_Gallery',
            'WP_Widget_Custom_HTML',
            'Twenty_Eleven_Ephemera_Widget',
            'WP_Widget_Categories',
            'WP_Widget_Tag_Cloud',
            'WP_Widget_Recent_Posts'
        );
        $app_setup->set_unregister_default_widgets( $unregister_default_widgets );
        $app_setup->set_post_formats( $post_formats = array( 'video', 'gallery' ) ); 
        date_default_timezone_set( 'Europe/Madrid' );
        add_action( 'after_setup_theme', array( $app_setup, 'init' ) );
        if ( ENVIRONMENT === 'pre' || ENVIRONMENT === 'pro' ) {
            add_filter( 'acf/settings/show_admin', '__return_false' );
        }
    }

    /**
     * media_settings function
     *
     * Set media settings
     * Method set_custom_logo with [array] params [int] width and [int] height
     * Method set_image_extra with array bidimensional [string] name of image_extra and other array with params for each image [int] width, [int] height and [bool] crop
     *
     * @return object NwpMedia
     */
    private function media_settings()
    {
        $app_media = new NwpMedia;
        $app_media->set_custom_logo(
            $custom_logo = array(
                'width'     => '128',
                'height'    => '128'
            )
        );
        return $app_media;
    }

    /**
     * public_assets_init function
     *
     * Settings for public assets, css, js, jquery
     * Set [array] $app_css_files in array with [string] name and file for enqueue
     * Set [strign] $app_jquery for use another version than WordPress is using or self-hosted version
     * Set [array] $app_js_files in array with [string] name and file for enqueue them, these files has dependency of jquery defined like "jquery"
     * Set [array] $app_ajax_var array with [string] handle and [string] object
     *
     * @return void
     */
    private function public_assets_init()
    {
        $app_css_files = array(
            'nwp-vendor'            => ASSETS_PUBLIC_PATH . 'css/vendors.min.css',
            'nwp-styles'            => ASSETS_PUBLIC_PATH . 'css/styles.min.css',
        );
        $app_js_files = array(
            'nwp-scripts'           => ASSETS_PUBLIC_PATH . 'js/scripts.min.js',
            'nwp-components'        => ASSETS_PUBLIC_PATH . 'js/components.min.js',
        );
        $app_ajax_var = array(
            'handle'    => '',
            'object'    => '',
        );
        $app_public_assets = new NwpAssets( $this->app_version, $this->app_css_version, $this->app_js_version );
        $app_public_assets->set_css_files( $app_css_files );
        $app_public_assets->set_js_files( $app_js_files );
        $app_public_assets->set_ajax_var( $app_ajax_var );
        add_action( 'wp_enqueue_scripts', array( $app_public_assets, 'enqueue_styles' ) );
        add_action( 'wp_enqueue_scripts', array( $app_public_assets, 'enqueue_js' ) );
        add_action( 'wp_enqueue_scripts', array( $app_public_assets, 'add_ajax_var' ) );
    }

    /**
     * dashboard_init function
     *
     * @return void
     */
    private function dashboard_init()
    {
        $app_dashboard = new NwpDashboard( $this->app_version, false );
        $app_dashboard->set_nwp_widget_enable( $app_nwp_widget_enable = true );
        add_action( 'after_setup_theme', array( $app_dashboard, 'init' ) );
    }

    /**
     * wpo_init function
     *
     * Instance NwpWPO class for Web Perfomance Optimization
     * Only available in ENVIRONMENT PRO or PRE
     * set [bool] $html_min to true for enable html minified but not javascript code
     * set [bool] $lazyload to true for enable Lazyload in images (featured image and content images)
     *
     * @return void
     */
    private function wpo_init()
    {
        $app_wpo = new NwpWPO;
        $app_wpo->set_html_min( $html_min = false );
        $app_wpo->set_lazyload( $lazyload = false );
        add_action( 'after_setup_theme', array( $app_wpo, 'init' ) );
    }

    /**
     * tinymce_plugins_init function
     * Instance NwpTinyMcePlugins class add plugins in tinymce editor
     * Only available in dashboard
     * set [array] $plugins with the name of plugin (list in vendor/TinyMcePlugins/plugins)
     *
     * @return void
     */
    private function tinymce_plugins_init()
    {
        if ( is_admin() ) {
            $plugins = array();
            $app_tinymce_plugins = new NwpTinyMcePlugins;
            $app_tinymce_plugins->set_plugins( $plugins );
            add_action( 'after_setup_theme', array( $app_tinymce_plugins, 'init' ) );
        }
    }

    /**
     * multilingual_press_init function
     * Instance NwpMultilingualPress class
     *
     * @return void
     */
    private function multilingual_press_init()
    {
        $multilingual_press = new NwpMultilingualPress;
        add_action( 'after_setup_theme', array( $multilingual_press, 'init' ) );
    }

    /**
     * helpers function
     *
     * @return void
     */
    private function helpers()
    {
        require_once get_template_directory() . '/src/Helpers/helpers.php';
    }

    /**
     * navwalker_init function
     *
     * @return void
     */
    private function navwalker_init()
    {
        $navwalker = new Component\MenuNavWalker\MyMenuNavWalker;
        add_action( 'init', array( $navwalker, 'init' ) );
    }

    /**
     * shortcodes_init function
     *
     * @return void
     */
    private function shortcodes_init()
    {
        $shortcodes = new Component\Shortcodes\MyShortcodes;
        add_action( 'after_setup_theme', array( $shortcodes, 'init' ) );
    }

    /**
     * customizer_init function
     *
     * @return void
     */
    private function customizer_init()
    {
        $customizer = new Component\Customizer\MyCustomizer;
        add_action( 'after_setup_theme', array( $customizer, 'init' ) );
    }

    /**
     * posts_init function
     *
     * @return void
     */
    private function posts_init()
    {
        $posts = new Component\Posts\MyPosts;
        add_action( 'after_setup_theme', array( $posts, 'init' ) );
    }

    /**
     * pages_init function
     *
     * @return void
     */
    private function pages_init()
    {
        $pages = new Component\Pages\MyPages;
        add_action( 'after_setup_theme', array( $pages, 'init' ) );
    }

    /**
     * front_page_init function
     *
     * @return void
     */
    private function front_page_init()
    {
        $front_page = new Component\FrontPage\MyFrontPage;
        add_action( 'after_setup_theme', array( $front_page, 'init' ) );
    }
}