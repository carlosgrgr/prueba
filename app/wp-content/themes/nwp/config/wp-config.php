<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

 /**
 * Define application enviroment.
 *
 * Used unto project to switch between functionalities depending on the environment.
 * Values accepted: local/dev/pre/pro.
 */
define( 'ENVIRONMENT', 'local' );

switch ( ENVIRONMENT )
{
	case 'local':
		error_reporting(-1);
		ini_set('display_errors', 1);
		$db_settings = array(
			'db_name' 		=> 'coperamadb',
			'db_user' 		=> '',
			'db_password' 	=> '',
			'db_host'		=> '',
			'db_prefix'		=> 'cop_'
		);
		$protocol = 'http';
		define('WP_DEBUG', true);
		break;
	case 'dev':
		error_reporting(-1);
		ini_set('display_errors', 1);
		$db_settings = array(
			'db_name' 		=> '',
			'db_user' 		=> '',
			'db_password' 	=> '',
			'db_host'		=> '',
			'db_prefix'		=> 'ntv_'
		);
		$protocol = 'http';
		define('WP_DEBUG', true);
		break;
	case 'pre':
		error_reporting(-1);
		ini_set('display_errors', 1);
		$db_settings = array(
			'db_name' 		=> '',
			'db_user' 		=> '',
			'db_password' 	=> '',
			'db_host'		=> '',
			'db_prefix'		=> 'ntv_'
		);
		$protocol = 'http';
		define('WP_DEBUG', true);
		break;
	case 'pro':
		ini_set('display_errors', 0);
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
		$db_settings = array(
			'db_name' 		=> '',
			'db_user' 		=> '',
			'db_password' 	=> '',
			'db_host'		=> '',
			'db_prefix'		=> 'ntv_'
		);
		$protocol = 'https';
		define('WP_DEBUG', false);
		break;
	default:
		define('WP_DEBUG', false);
		header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
		echo 'The application environment is not set correctly.';
		exit(1); // EXIT_ERROR
}


// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', $db_settings['db_name']);

/** Tu nombre de usuario de MySQL */
define('DB_USER', $db_settings['db_user']);

/** Tu contraseña de MySQL */
define('DB_PASSWORD', $db_settings['db_password']);

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', $db_settings['db_host']);

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', 'utf8_general_ci');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'y@_s?t{CW:ZXUgI9[s,BFw)]?{L>1-6[AOuH}*J@PW!BhWpW$eA.h7rq3pGRD-J)');
define('SECURE_AUTH_KEY',  'j^-mXrn(}:0e. jp;Zo)),q1g=wD,|x-?6Lu40yY<q#,]@hBGUcH}h>+Gi&CbUf(');
define('LOGGED_IN_KEY',    'UN(~|TkH&*!xz$7h@5,R^-6%RX{vLj+YZH|Z0djc4zP?O?w%r#@K.c@^l@H&Q&[9');
define('NONCE_KEY',        'Bl+*{:_qS{Wra:4<4WU*Ek%/!*Zb!lH`vgT+#S%L;b-Z+,#1@mSUVB}r3o0WZL=#');
define('AUTH_SALT',        ':m*At4V35yD2~=M;Am)cV#Bv?{cA|v&(uxDIS1N$_6|6Od}H40tkF5G4LG#fMlQn');
define('SECURE_AUTH_SALT', 'O$wz-Z$!s|K/DLMm-rsE&8wT,72N_{ <*+rab}taoZf&twGC1Q/gWq%bKYc^+@YQ');
define('LOGGED_IN_SALT',   '&B-,D$`0K|X8#Zx2^yH!Uk4Q2{4C[$LL-AX&p^>|G`X5{n))Iv?QLIq5MqL5+L7=');
define('NONCE_SALT',       'A0u:}BPjzn!lT1$[fY@I<8q8`G}_J=|xKe![4r%-@p16Q&Qwu/FhSd?hda2Ha4YU');

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = $db_settings['db_prefix'];

/* Compression */
define('COMPRESS_CSS',        true);
define('COMPRESS_SCRIPTS',    true);
define('CONCATENATE_SCRIPTS', true);
define('ENFORCE_GZIP',        true);

define( 'WPMU_PLUGIN_DIR', dirname( __FILE__ ) . '/wp-content/nwp-plugins' );
define( 'WPMU_PLUGIN_URL', $protocol . '://' . $_SERVER['HTTP_HOST'] . '/wp-content/nwp-plugins');
		
/* ¡Eso es todo, deja de editar! Feliz blogging */
//define('WP_ALLOW_MULTISITE', true);
//define('MULTISITE', true);
//define('SUBDOMAIN_INSTALL', false);
//define('DOMAIN_CURRENT_SITE', 'new.nateevo.com');
//define('PATH_CURRENT_SITE', '/blog/');
//define('SITE_ID_CURRENT_SITE', 1);
//define('BLOG_ID_CURRENT_SITE', 1);
//define('SUNRISE', 'on');

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');