<?php
/**
 * Nateevo Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package nwp_theme
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

require_once 'vendor/autoload.php';

use App\MyApp;
$app = new MyApp;
$app->init();
