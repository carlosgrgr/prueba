<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package nwp_theme
 */
get_header();
get_template_part( 'template-parts/content', 'search' );
get_footer();