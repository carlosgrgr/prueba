<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nwp_theme
 */
?>
    </main>
        <?php get_template_part( 'template-parts/footer/content', 'footer' );
        wp_footer(); ?>
    </body>
</html>