<?php
namespace App\Component\Cookies;

if ( ! defined( 'ABSPATH' ) ) { exit; }

use WP_Customize_Manager;
use WP_Customize_Upload_Control;
use WP_Customize_Image_Control;
use WP_Customize_Color_Control;
use WP_Customize_Media_Control;

class MyCookies
{
    public function __construct()
    {
    }

    public function init()
    {
        $this->init_actions();
        $this->init_filters();
    }

    public function init_actions()
    {
        add_action( 'customize_register', array( $this, 'add_cookie_consent_options' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_cookies_css_js' ) );
    }

    public function init_filters()
    {
    }

    public function add_cookie_consent_options( $wp_customize )
    {
        // Cookie consent settings options
        $wp_customize->add_section( 'my_cookie_consent_section' , array(
            'title'         => __( 'Aviso de cookies', APP_TEXTDOMAIN ),
            'priority'      => 162,
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_setting( 'my_cookie_consent_title', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_cookie_consent_title', array(
            'label'     => __( 'Título', APP_TEXTDOMAIN ),
            'section'   => 'my_cookie_consent_section',
            'priority'  => 1,
            'type'      => 'text',
        ) );

        $wp_customize->add_setting( 'my_cookie_consent_text', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_cookie_consent_text', array(
            'label'     => __( 'Texto', APP_TEXTDOMAIN ),
            'section'   => 'my_cookie_consent_section',
            'priority'  => 2,
            'type'      => 'textarea',
        ) );

        $wp_customize->add_setting( 'my_cookie_consent_page', array(
            'type'              => 'option',
            'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'absint'
        ) );

        $wp_customize->add_control( 'my_cookie_consent_page', array(
            'label'     => __( 'Página para el consentimiento de cookies', APP_TEXTDOMAIN ),
            'section'   => 'my_cookie_consent_section',
            'priority'  => 3,
            'type'      => 'dropdown-pages',
        ) );

        $wp_customize->add_setting( 'my_cookie_consent_more_info', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_cookie_consent_more_info', array(
            'label'     => __( 'Texto enlace «Más información»', APP_TEXTDOMAIN ),
            'section'   => 'my_cookie_consent_section',
            'priority'  => 4,
            'type'      => 'text',
        ) );

        $wp_customize->add_setting( 'my_cookie_consent_accept', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_cookie_consent_accept', array(
            'label'     => __( 'Texto enlace «Aceptar»', APP_TEXTDOMAIN ),
            'section'   => 'my_cookie_consent_section',
            'priority'  => 5,
            'type'      => 'text',
        ) );
    }

    public function enqueue_cookies_css_js()
    {
        wp_enqueue_style( 'cookies-css', APP_PATH . 'src/Cookies/css/cookies.css' );
        wp_register_script( 'cookies-js', APP_PATH . 'src/Cookies/js/cookies.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'cookies-js' );
    }
}
