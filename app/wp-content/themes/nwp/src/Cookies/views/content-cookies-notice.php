<?php
$title     = get_option( 'my_cookie_consent_title' );
$title     = $title ? $title : __( 'Utilizamos cookies', APP_TEXTDOMAIN );
$text      = get_option( 'my_cookie_consent_text' );
$text      = $text ? $text : __( 'Por favor, acepta las cookies para continuar disfrutando de nuestra web', APP_TEXTDOMAIN );
$page      = get_option( 'my_cookie_consent_page' );
$page      = $page ? get_permalink( $page ) : '#';
$more_info = get_option( 'my_cookie_consent_more_info' );
$more_info = $more_info ? $more_info : __( 'Más información', APP_TEXTDOMAIN );
$accept    = get_option( 'my_cookie_consent_accept' );
$accept    = $accept ? $accept : __( 'Aceptar las cookies', APP_TEXTDOMAIN );
?>

<div class="alert alert-dismissible cookiealert show" role="alert">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cookiealert-container"> 
                    <div class="cookie-text">
                        <p class="title"><?php echo $title ?></p>
                        <p><?php echo $text ?> <a class="link-decoration" href="<?php echo $page ?>" target="_blank"><?php echo $more_info ?></a></p>
                    </div>
                    <div class="cookie-button">
                        <div class="wrap-btn-principal">
                            <button class="acceptcookies" type="button" aria-label="Close"><?php echo $accept ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>