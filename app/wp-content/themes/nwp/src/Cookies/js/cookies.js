$(document).ready(function() {
	/* COOKIES */
	if($('.cookiealert').length > 0) {
	  var cookieAlert = document.querySelector(".cookiealert");
	  var acceptCookies = document.querySelector(".acceptcookies");

	  cookieAlert.offsetHeight; // Force browser to trigger reflow (https://stackoverflow.com/a/39451131)

	  if (!getCookie("acceptCookies")) {
		  cookieAlert.classList.add("show");
	  }

	  acceptCookies.addEventListener("click", function() {
		  setCookie("acceptCookies", true, 365);
		  cookieAlert.classList.remove("show");
	  });


	  // Cookie functions stolen from w3schools
	  function setCookie(cname, cvalue, exdays) {
		  var d = new Date();
		  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		  var expires = "expires=" + d.toUTCString();
		  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	  }

	  function getCookie(cname) {
		  var name = cname + "=";
		  var decodedCookie = decodeURIComponent(document.cookie);
		  var ca = decodedCookie.split(';');
		  for (var i = 0; i < ca.length; i++) {
			  var c = ca[i];
			  while (c.charAt(0) === ' ') {
				  c = c.substring(1);
			  }
			  if (c.indexOf(name) === 0) {
				  return c.substring(name.length, c.length);
			  }
		  }
		  return "";
	  }


	  $('.cookiealert button').on('click', function(e) {
		e.preventDefault();
        $('.cookiealert').addClass('d-none');
        $('header').addClass('no-cookies');

		$('.hero-video__icons').addClass('no-cookies');
		$('.page-inner').addClass('no-cookies');
	  });

	  /* FIN COOKIES */
	};
});