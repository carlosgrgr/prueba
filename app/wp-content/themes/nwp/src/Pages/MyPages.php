<?php
namespace App\Component\Pages;

if ( ! defined( 'ABSPATH' ) ) { exit; }

class MyPages
{
    public function __construct()
    {
    }

    public function init()
    {
        $this->init_actions();
        $this->init_filters();
    }

    public function init_actions()
    {
        add_action( 'init', array( $this, 'page_fields' ) );
        add_action( 'init', array( $this, 'nwp_pages_support' ) );
    }

    public function init_filters()
    {
        add_filter( 'theme_page_templates', array( $this, 'init_page_templates' ), 99, 1 );   
    }

    public function init_page_templates( $post_templates )
    {
        $post_templates += array(
            '/src/Pages/views/template-page.php'            => 'Plantilla Full Width',
            '/src/Pages/views/template-page-sidebar.php'    => 'Plantilla con Sidebar',
            //'path' => 'Label'; 
        );
        return $post_templates;
    }

    public function page_fields()
    {
    }

    public function nwp_pages_support()
    {
        add_post_type_support( 'page', 'excerpt' );
    }
}
