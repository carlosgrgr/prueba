<?php
namespace App\Component\Widgets;

if ( ! defined( 'ABSPATH' ) ) { exit; }

class MyWidgets
{
    public function __construct()
    {
    }

    public function init()
    {
        $this->init_actions();
        $this->init_filters();
    }

    public function init_actions()
    {   
        add_action( 'widgets_init', array( $this, 'register_widgets' ) );
        add_action( 'init', array( $this, 'add_custom_theme_support' ) );
    }

    public function init_filters()
    {
    }

    public function add_custom_theme_support()
	{
        add_theme_support( 'customize-selective-refresh-widgets' );
    }
    
    public function register_widgets()
    {
    }
}
