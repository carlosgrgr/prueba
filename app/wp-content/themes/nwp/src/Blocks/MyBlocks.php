<?php
namespace App\Component\Blocks;

if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
 * Master class to register a gutenberg block
 *
 * @uses acf_add_local_field_group()
 * @see https://support.advancedcustomfields.com/forums/topic/acf_add_local_field_group-vs-register_field_group/
 */
class MyBlocks
{
    public function __construct()
    {
    }

    public function init()
    {
        $this->init_blocks();
        $this->init_actions();
    }

    public function init_blocks()
    {
        new ExampleBlock\MyExampleBlock;
    }

    public function init_actions()
    {
        add_action( 'enqueue_block_editor_assets', array( $this, 'enqueue_assets' ) );
    }

    public function enqueue_assets()
    {
        wp_enqueue_style( 'global-general-editor-css', ASSETS_PUBLIC_PATH . 'css/style-editor.css' );
    }

    static public function register_acf_field_group( array $args = array() )
    {
        $defaults = array (
            'menu_order'            => 0,
            'position'              => 'side',
            'style'                 => 'default',
            'label_placement'       => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen'        => '',
            'active'                => true,
            'description'           => '',
        );
        if( function_exists( 'acf_add_local_field_group' ) )
            acf_add_local_field_group( wp_parse_args( $args, $defaults ) );

    }
}