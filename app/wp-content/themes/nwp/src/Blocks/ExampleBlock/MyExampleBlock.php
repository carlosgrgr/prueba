<?php
namespace App\Component\Blocks\ExampleBlock;

use \App\Component\Blocks\MyBlocks;

if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
 * Class to register a gutenberg block
 *
 * @uses MyBlocks::register_acf_field_group Add local field group.
 * @uses acf_register_block_type()
 * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
 *
 * @param string $name Name to be registered as a gutenberg block.
 * @param string $title Name to display on UI.
 * @param string $description Description to display on UI.
 */
 class MyExampleBlock
{
    public $name        = '';
    public $title       = '';
    public $description = '';
    
    public function __construct()
    {
        $this->init_actions();
        $this->init_filters();
    }

    public function init_actions()
    {
        // add_action( 'acf/init', array( $this, 'register_block' ) );
        // add_action( 'init', array( $this, 'register_acf_field_group' ) );
    }

    public function init_filters()
    {
    }

    public function register_block()
    {
        if( function_exists( 'acf_register_block_type' ) ) {
            acf_register_block_type(array(
                'name'				=> $this->name,
                'title'				=> __( $this->title, APP_TEXTDOMAIN ),
                'description'		=> __( $this->description, APP_TEXTDOMAIN ),
                'render_template'	=> '',
                'category'			=> '',
                'icon'				=> '',
                'supports'          => array(
                    'align' => array( 'full' ),
                ),
                'keywords'			=> array( '' ),
                'post_types'        => array( 'page' ),
                'enqueue_assets'    => function() {
                    wp_enqueue_style( 'block_' . $this->name, get_template_directory_uri() . '' );
                    wp_enqueue_script( 'block_' . $this->name.'_script', get_template_directory_uri() . '', array( 'jquery' ), '', true );
                },
            ));
        }
    }

    public function register_acf_field_group()
    {
        $args = array(
            'key' => 'group_block_' . $this->name,
            'title' => __( 'Block: ' . $this->title, APP_TEXTDOMAIN ),
            'fields' => array(),
            'location' => array(
                array(
                    array(
                        'param' => 'block',
                        'operator' => '==',
                        'value' => 'acf/' . $this->name,
                    ),
                ),
            ),
        );
        MyBlocks::register_acf_field_group( $args );
    }
}