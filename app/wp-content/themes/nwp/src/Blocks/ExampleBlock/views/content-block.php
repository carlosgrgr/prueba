<?php
/**
 * Block Name: Example
 * This is the template that displays the gutengerb block.
 * 
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param  int|string $post_id The post ID this block is saved to. 
 * 
 * @see https://www.advancedcustomfields.com/blog/acf-5-8-introducing-acf-blocks-for-gutenberg/
 */

if ( is_admin() ) : 
    set_query_var( 'block', $block );
    get_template_part( 'template-parts/blocks/content-block', 'editor' );
else :
    get_template_part( 'src/Blocks/ExampleBlock/views/layouts/layout', 'front' );
endif;
