<?php
namespace App\Component\FrontPage;

if ( ! defined( 'ABSPATH' ) ) { exit; }

class MyFrontPage
{
    public function __construct()
    {
    }

    public function init()
    {
        $this->init_actions();
        $this->init_filters();
    }

    public function init_actions()
    {
    }

    public function init_filters()
    {
        add_filter( 'allowed_block_types', array( $this, 'set_front_page_allowed_guntenberg_blocks' ), 11, 2 );
    }

    public function set_front_page_allowed_guntenberg_blocks( $blocks, $post )
	{
		if ( $post->ID == get_option( 'page_on_front' ) ) {
			$blocks = array(
                'core/paragraph',
                'core/heading',
                'acf/header-home',
                'acf/information',
                'acf/baner-boxes',
                'acf/client-logos',
                'acf/testimonials',
                'acf/featured-boxes',
            );
		}
		return $blocks;
	}
}
