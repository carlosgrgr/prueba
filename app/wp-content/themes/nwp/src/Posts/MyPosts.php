<?php
namespace App\Component\Posts;

if ( ! defined( 'ABSPATH' ) ) { exit; }

class MyPosts
{
    public function __construct()
    {
    }

    public function init()
    {
        $this->init_actions();
        $this->init_filters();
    }

    public function init_actions()
    {
    }

    public function init_filters()
    {
    }
}