<?php the_content(); ?>
<?php $areas = get_the_terms( $post->ID, 'area' ); 
foreach ( $areas as $area ) :
echo '<a href="'.get_term_link( $area->term_id ).'">'.$area->name.'</a>';
endforeach; ?>