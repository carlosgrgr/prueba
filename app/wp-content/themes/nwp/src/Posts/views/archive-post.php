<section>
    <h2 class="title"><?php _e( 'Últimas publicaciones sobre ', APP_TEXTDOMAIN ); echo get_queried_object()->name; ?></h2>
    <?php while( $wp_query->have_posts() ) : $wp_query->the_post();
        get_template_part( 'template-parts/content', 'excerpt' );
    endwhile; ?>
</section>