<?php
namespace App\Component\SMTPSettings;

if ( ! defined( 'ABSPATH' ) ) { exit; }

use WP_Customize_Manager;
use WP_Customize_Upload_Control;
use WP_Customize_Image_Control;
use WP_Customize_Color_Control;
use WP_Customize_Media_Control;

class MySMTPSettings
{
    public function __construct()
    {
    }

    public function init()
    {
        $this->init_actions();
    }

    public function init_actions()
    {
        add_action( 'customize_register', array( $this, 'add_smtp_settings' ) );
        add_action( 'phpmailer_init', array( $this, 'add_smtp_to_wpmail' ), 99999 );
    }

    public function add_smtp_settings( $wp_customize )
    {
        //SMTP Settings
        $wp_customize->add_section( 'my_smtp_settings' , array(
            'title' => __( 'Ajustes SMTP', APP_TEXTDOMAIN ),
            'priority' => 190,
            'description' => __( 'Establece los ajustes SMTP de la cuenta que enviará los mails.', APP_TEXTDOMAIN ),
        ));
        //Servidor
        $wp_customize->add_setting( 'my_smtp_server', array(
            'type' => 'option',
            'capability' => 'edit_theme_options',
        ) );
        $wp_customize->add_control( 'my_smtp_server', array(
            'label' => __( 'Host', APP_TEXTDOMAIN ),
            'description' => __( 'Servidor de correo saliente', APP_TEXTDOMAIN ),
            'section' => 'my_smtp_settings',
            'priority' => 1,
            'type' => 'text',
        ));
        //Puerto
        $wp_customize->add_setting( 'my_smtp_port', array(
            'type'      => 'option',
            'capability'=> 'edit_theme_options',
        ) );
        $wp_customize->add_control( 'my_smtp_port', array(
            'label'     => __( 'Puerto', APP_TEXTDOMAIN ),
            'section'   => 'my_smtp_settings',
            'priority'  => 2,
            'type'      => 'text',
        ));
        //Username
        $wp_customize->add_setting( 'my_smtp_username', array(
            'type'      => 'option',
            'capability'=> 'edit_theme_options',
        ) );
        $wp_customize->add_control( 'my_smtp_username', array(
            'label'     => __( 'Usuario', APP_TEXTDOMAIN ),
            'section'   => 'my_smtp_settings',
            'priority'  => 3,
            'type'      => 'text',
        ));
        //Clave
        $wp_customize->add_setting( 'my_smtp_password', array(
            'type'      => 'option',
            'capability'=> 'edit_theme_options',
        ) );
        $wp_customize->add_control( 'my_smtp_password', array(
            'label'     => __( 'Clave', APP_TEXTDOMAIN ),
            'section'   => 'my_smtp_settings',
            'priority'  => 4,
            'type'      => 'password',
        ));
        //Encriptación
        $wp_customize->add_setting( 'my_smtp_encryption', array(
            'type'      => 'option',
            'capability'=> 'edit_theme_options',
        ) );
        $wp_customize->add_control( 'my_smtp_encryption', array(
            'label'     => __( 'Encriptación', APP_TEXTDOMAIN ),
            'section'   => 'my_smtp_settings',
            'priority'  => 5,
            'type'      => 'select',
            'choices'   => array( '0' => 'None', 'ssl' => 'SSL', 'tls' => 'TLS' )
        ));
        //From
        $wp_customize->add_setting( 'my_smtp_from', array(
            'type'      => 'option',
            'capability'=> 'edit_theme_options',
        ) );
        $wp_customize->add_control('my_smtp_from', array(
            'label'     => __( 'From', APP_TEXTDOMAIN ),
            'section'   => 'my_smtp_settings',
            'priority'  => 6,
            'type'      => 'text',
        ));
        //From Name
        $wp_customize->add_setting( 'my_smtp_fromname', array(
            'type'      => 'option',
            'capability'=> 'edit_theme_options',
        ) );
        $wp_customize->add_control('my_smtp_fromname', array(
            'label'     => __( 'From Name', APP_TEXTDOMAIN ),
            'section'   => 'my_smtp_settings',
            'priority'  => 7,
            'type'      => 'text',
        ));
    }

    public function add_smtp_to_wpmail( $phpmailer )
    {
        global $phpmailer;
        $smtp_settings = array(
            'server'        => get_option( 'my_smtp_server' ),
            'port'          => ( $option = get_option( 'my_smtp_port' ) ) ? $option : 25,
            'username'      => get_option( 'my_smtp_username' ),
            'password'      => get_option( 'my_smtp_password' ),
            'encryption'    => get_option( 'my_smtp_encryption' ),
            'from'          => get_option( 'my_smtp_from' ),
            'fromname'      => get_option( 'my_smtp_fromname' ),
            'mailer'        => 'smpt'
        );
        // If server name or password are empty, don't touch anything!
        if ( empty( $smtp_settings['server'] ) || empty( $smtp_settings['username'] ) || empty( $smtp_settings['password'] ) ) { return; }
    
        $phpmailer->Mailer      = $smtp_settings['mailer'];
        $phpmailer->SMTPAuth    = true; // Always use authentication. I don't support open relays!
        $phpmailer->Host        = $smtp_settings['server'];
        $phpmailer->Port        = $smtp_settings['port'] ;
        $phpmailer->Username    = $smtp_settings['username'];
        $phpmailer->Password    = $smtp_settings['password'];
        $phpmailer->SMTPSecure  = $smtp_settings['encryption'];
        $phpmailer->From        = $smtp_settings['from'];
        $phpmailer->FromName    = $smtp_settings['fromname'];
    }
}