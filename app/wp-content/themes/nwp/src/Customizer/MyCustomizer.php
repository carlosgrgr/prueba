<?php
namespace App\Component\Customizer;

if ( ! defined( 'ABSPATH' ) ) { exit; }

use WP_Customize_Manager;
use WP_Customize_Upload_Control;
use WP_Customize_Image_Control;
use WP_Customize_Color_Control;
use WP_Customize_Media_Control;

class MyCustomizer
{
    public function __construct()
    {
    }

    public function init()
    {
        $this->init_actions();
        $this->init_filters();
    }

    public function init_actions()
    {
        add_action( 'admin_init', array( $this, 'add_editor_role_access' ) );
        add_action( 'customize_register', array( $this, 'remove_default_options' ), 30 );
        add_action( 'customize_register', array( $this, 'add_analytics' ) );
        add_action( 'customize_register', array( $this, 'add_social_networks' ) );
        add_action( 'customize_register', array( $this, 'add_twitter_box_panel' ) );
        add_action( 'customize_register', array( $this, 'add_contact_section' ) );
        add_action( 'customize_register', array( $this, 'add_identity_options' ), 0 );
        add_action( 'customize_register', array( $this, 'add_404_options' ) );
        add_action( 'customize_register', array( $this, 'add_footer_options' ) );
    }

    public function init_filters()
    {
        add_filter( 'customize_loaded_components', array( $this, 'remove_widgets_panel' ) );
        add_filter( 'customize_loaded_components', array( $this, 'remove_menu_panel' ) );
    }

    public function add_editor_role_access()
    {
        $roleObject = get_role( 'editor' );
        if ( ! $roleObject->has_cap( 'edit_theme_options' ) ) {
            $roleObject->add_cap( 'edit_theme_options' );
        }
    }

    public function remove_default_options( $wp_customize )
    {
        $components = array(
            'static_front_page',
            'colors',
            'header_image',
            'background_image',
            //'nav_menus',
            'themes',
            'featured_content',
            //'widgets',
            'custom_css',
            'fl-header-logo',
            'fl-header-logo',
        );
        $controls  = array(
            // 'custom_logo',
        );
        foreach ( $components as $component ) {
            $wp_customize->remove_section( $component );
        }
        foreach ( $controls as $control ) {
            $wp_customize->remove_control( $control );
        }
    }

    public function remove_widgets_panel( $components )
    {
        $i = array_search( 'widgets', $components );
        if ( false !== $i ) {
            unset( $components[ $i ] );
        }
        return $components;
    }

    public function remove_menu_panel( $components )
    {
        $i = array_search( 'nav_menus', $components );
        if ( false !== $i ) {
            unset( $components[ $i ] );
        }
        return $components;
    }

    public function add_analytics( $wp_customize )
    {
        // Analytics Section
        $wp_customize->add_section( 'my_analytics_section' , array(
            'title'         => __( 'Código de Google Analytics', APP_TEXTDOMAIN ),
            'priority'      => 160,
            'capability'    => 'edit_pages',
        ) );

        // Analytics
        $wp_customize->add_setting( 'my_analytics_code', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_analytics_code', array(
            'label'     => __( 'Código de Google Analytics', APP_TEXTDOMAIN ),
            'section'   => 'my_analytics_section',
            'priority'  => 1,
            'type'      => 'textarea',
        ) );
    }

    public function add_social_networks( $wp_customize )
    {
        // Social Networks Section
        $wp_customize->add_section( 'my_social_section' , array(
            'title'         => __( 'Redes Sociales', APP_TEXTDOMAIN ),
            'priority'      => 163,
            'capability'    => 'edit_pages',
        ) );

        // Redes Sociales: Instagram
        $wp_customize->add_setting( 'my_instagram_url', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );
        $wp_customize->add_control( 'my_instagram_url', array(
            'label'     => __( 'Instagram url', APP_TEXTDOMAIN ),
            'section'   => 'my_social_section',
            'priority'  => 3,
            'type'      => 'text',
        ) );

        // Redes Sociales: LinkedIn
        $wp_customize->add_setting( 'my_linkedin_url', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );
        $wp_customize->add_control( 'my_linkedin_url', array(
            'label'     => __( 'LinkedIn url', APP_TEXTDOMAIN ),
            'section'   => 'my_social_section',
            'priority'  => 2,
            'type'      => 'text',
        ) );

        // Redes Sociales: Youtube
        $wp_customize->add_setting( 'my_youtube_url', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );
        $wp_customize->add_control( 'my_youtube_url', array(
            'label'     => __( 'Youtube url', APP_TEXTDOMAIN ),
            'section'   => 'my_social_section',
            'priority'  => 3,
            'type'      => 'text',
        ) );
        
        // Redes Sociales: Facebook
        $wp_customize->add_setting( 'my_facebook_url', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );
        $wp_customize->add_control( 'my_facebook_url', array(
            'label'     => __( 'Facebook url', APP_TEXTDOMAIN ),
            'section'   => 'my_social_section',
            'priority'  => 1,
            'type'      => 'text',
        ) );
    }

    public function add_twitter_box_panel()
    {
        global $wp_customize;
        
        //Twitter Account Sections
        $wp_customize->add_section( 'twitter_section' , array(
            'title'         => __( 'Tokens de Twitter', APP_TEXTDOMAIN ),
            'priority'      => 163,
            'capability'    => 'edit_pages',
        ) );

        // OAuth Access Token
        $wp_customize->add_setting( 'my_twitter_oauth_access_token', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_twitter_oauth_access_token', array(
            'label'     => __( 'Token de acceso', APP_TEXTDOMAIN ),
            'section'   => 'twitter_section',
            'priority'  => 1,
            'type'      => 'text',
        ) );

        // OAuth Access Token Secret
        $wp_customize->add_setting( 'my_twitter_oauth_access_token_secret', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_twitter_oauth_access_token_secret', array(
            'label'     => __( 'Token de acceso secreto', APP_TEXTDOMAIN ),
            'section'   => 'twitter_section',
            'priority'  => 2,
            'type'      => 'text',
        ) );

        // Consumer Key
        $wp_customize->add_setting( 'my_twitter_consumer_key', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_twitter_consumer_key', array(
            'label'     => __( 'Clave API', APP_TEXTDOMAIN ),
            'section'   => 'twitter_section',
            'priority'  => 3,
            'type'      => 'text',
        ) );

        // Consumer Secret
        $wp_customize->add_setting( 'my_twitter_consumer_secret', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_twitter_consumer_secret', array(
            'label'     => __( 'Clave API secreta', APP_TEXTDOMAIN ),
            'section'   => 'twitter_section',
            'priority'  => 4,
            'type'      => 'text',
        ) );
    }

    public function add_contact_section( $wp_customize )
    {
        // Contact Section
        $wp_customize->add_section( 'my_contact_section' , array(
            'title'         => __( 'Contacto', APP_TEXTDOMAIN ),
            'priority'      => 163,
            'capability'    => 'edit_pages',
        ) );

        // Contact Section: email
        $wp_customize->add_setting( 'my_contact_email', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );
        $wp_customize->add_control( 'my_contact_email', array(
            'label'     => __( 'Correo electrónico', APP_TEXTDOMAIN ),
            'section'   => 'my_contact_section',
            'priority'  => 1,
            'type'      => 'text',
        ) );

        // Contact Section: phone
        $wp_customize->add_setting( 'my_contact_phone', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );
        $wp_customize->add_control( 'my_contact_phone', array(
            'label'     => __( 'Número telefónico', APP_TEXTDOMAIN ),
            'section'   => 'my_contact_section',
            'priority'  => 2,
            'type'      => 'text',
        ) );
    }

    public function add_identity_options( $wp_customize )
    {
        $wp_customize->add_setting( 'my_desktop_logo', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'my_desktop_logo', array(
            'label'      => __( 'Logo desktop', APP_TEXTDOMAIN ),
            'section'    => 'title_tagline',
        ) ) );
    }

    public function add_404_options( $wp_customize )
    {
        // Analytics Section
        $wp_customize->add_section( 'my_404_section' , array(
            'title'         => __( 'Página 404', APP_TEXTDOMAIN ),
            'priority'      => 168,
            'capability'    => 'edit_pages',
        ) );

        // Analytics
        $wp_customize->add_setting( 'my_404_code', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_404_code', array(
            'label'     => __( 'Texto informativo página 404', APP_TEXTDOMAIN ),
            'section'   => 'my_404_section',
            'priority'  => 1,
            'type'      => 'textarea',
        ) );
    }

    public function add_footer_options( $wp_customize )
    {
        // Footer Section
        $wp_customize->add_section( 'my_footer_section' , array(
            'title'         => __( 'Footer', APP_TEXTDOMAIN ),
            'priority'      => 169,
            'capability'    => 'edit_pages',
        ) );

        // Title Contact section
        $wp_customize->add_setting( 'my_footer_section_title', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_footer_section_title', array(
            'label'     => __( 'Title', APP_TEXTDOMAIN ),
            'section'   => 'my_footer_section',
            'priority'  => 1,
            'type'      => 'text',
        ) );

        // Mail Contact section
        $wp_customize->add_setting( 'my_footer_section_mail', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_footer_section_mail', array(
            'label'     => __( 'Mail', APP_TEXTDOMAIN ),
            'section'   => 'my_footer_section',
            'priority'  => 1,
            'type'      => 'text',
        ) );

        // Cellphone Contact section
        $wp_customize->add_setting( 'my_footer_section_tlf', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_footer_section_tlf', array(
            'label'     => __( 'Cellphone', APP_TEXTDOMAIN ),
            'section'   => 'my_footer_section',
            'priority'  => 1,
            'type'      => 'text',
        ) );

        // Message Contact section
        $wp_customize->add_setting( 'my_footer_section_message', array(
            'type'          => 'option',
            'capability'    => 'edit_pages',
        ) );

        $wp_customize->add_control( 'my_footer_section_message', array(
            'label'     => __( 'Message', APP_TEXTDOMAIN ),
            'section'   => 'my_footer_section',
            'priority'  => 1,
            'type'      => 'textarea',
        ) );
    }
}
