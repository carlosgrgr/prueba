<?php
namespace App\Component\MenuNavWalker;

if ( ! defined( 'ABSPATH' ) ) { exit; }

class MyMenuNavWalker
{
    public function __construct()
    {
    }

    public function init()
    {
        $this->init_filters();
    }

    public function init_actions()
    {
    }

    public function init_filters()
    {
        add_filter( 'nav_menu_css_class', array( $this, 'add_current_page_class' ), 10, 2 );
	}
	
	public function add_current_page_class( $classes, $item )
	{
		if ( in_array( 'current-menu-item', $classes ) ) {
			$classes[] = 'active ';
        }
        if ( in_array( 'current-menu-parent', $classes ) ) {
            $classes[] = 'active';
        }
        if ( in_array( 'mlp-current-language-item', $classes ) ) {
            $classes[] = 'active';
        }
		return $classes;
	}
}
