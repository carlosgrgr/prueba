<?php
/**
 * Helpers Functions.
 *
 * All utilities for this project.
 * ⚠️ Keep it as clean as possible.
 *
 * @package NWP
 */

function get_nwp_thumbnail_url( $args = array() )
{   
    $default = array( 'object_id' => get_the_ID(), 'object' => 'post', 'size' => 'full', 'url_default_image' => '' );
    $args = wp_parse_args( $args, $default ); 
    switch ( $args['object'] ) {
        case 'taxonomy':
            $image = get_term_meta( $args['object_id'], 'thumbnail_id', true );
            $image = wp_get_attachment_image_src( $image, $args['size'] );
            $image = empty( $image[0] ) ? ASSETS_PUBLIC_PATH.'images/default.png' : $image[0];
            break;
        case 'page':
            $image = ( $thumbnail_url = get_the_post_thumbnail_url( $args['object_id'], $args['size'] ) ) ? $thumbnail_url : $args['url_default_image'];
            $image = empty( $image ) ? ASSETS_PUBLIC_PATH.'images/default.png' : $image;
            break;
        default:
            $image = ( $thumbnail_url = get_the_post_thumbnail_url( $args['object_id'], $args['size'] ) ) ? $thumbnail_url : $args['url_default_image'];
            $image = empty( $image ) ? ASSETS_PUBLIC_PATH.'images/default.png' : $image;
            break;
    }
    return $image;
}

function nwp_pagination( $args = array() )
{    
    $defaults = array(
        'range'           => 4,
        'custom_query'    => FALSE,
        'previous_string' => '<em class="icon-arrow-left"></em>',
        'next_string'     => '<em class="icon-arrow-right"></em>',
        'before_output'   => '<div class="wrap-pagination pT0"><ul class="pagination">',
        'after_output'    => '</ul></div>'
    );
    $args = wp_parse_args( 
        $args, 
        apply_filters( 'wp_bootstrap_pagination_defaults', $defaults )
    );
    $args['range'] = ( int ) $args['range'] - 1;
    if ( !$args['custom_query'] )
        $args['custom_query'] = @$GLOBALS['wp_query'];
    $count = ( int ) $args['custom_query']->max_num_pages;
    $page  = intval( get_query_var( 'paged' ) );
    $ceil  = ceil( $args['range'] / 2 );
    if ( $count <= 1 )
        return FALSE;
    if ( !$page )
        $page = 1;
    if ( $count > $args['range'] ) {
        if ( $page <= $args['range'] ) {
            $min = 1;
            $max = $args['range'] + 1;
        } elseif ( $page >= ( $count - $ceil ) ) {
            $min = $count - $args['range'];
            $max = $count;
        } elseif ( $page >= $args['range'] && $page < ( $count - $ceil ) ) {
            $min = $page - $ceil;
            $max = $page + $ceil;
        }
    } else {
        $min = 1;
        $max = $count;
    }
    $echo = '';
    $previous = intval( $page ) - 1;
    $previous = esc_attr( get_pagenum_link( $previous ) );
    $firstpage = esc_attr( get_pagenum_link( 1 ) );
    /*if ( $firstpage && ( 1 != $page ) )
        $echo .= '<li><a href="' . $firstpage . '">' . __( 'Primera', APP_TEXTDOMAIN ) . '</a></li>';
    */
    if ( $page != 1 )
        $echo .= '<li><a href="'.$previous .'" title="'.__( 'anterior', APP_TEXTDOMAIN ).'">'.$args['previous_string'].'</a></li>';
    if ( !empty( $min ) && !empty( $max ) ) {
        for( $i = $min; $i <= $max; $i++ ) {
            if ( $page == $i ) {
                $echo .= '<li class="active"><a href="">'.str_pad( ( int )$i, 1, '', STR_PAD_LEFT ).'</a></li>';
            } else {
                $echo .= sprintf( '<li><a href="%s">%001d</a></li>', esc_attr( get_pagenum_link( $i ) ), $i );
            }
        }
    }
    $next = intval( $page ) + 1;
    if ( $count != $page ) { 
        $next = esc_attr( get_pagenum_link( $next ) ); 
        $echo .= '<li><a href="'.$next.'" title="'.__( 'siguiente', APP_TEXTDOMAIN ).'">'.$args['next_string'].'</a></li>';
    }
    /*$lastpage = esc_attr( get_pagenum_link( $count ) );
    if ( $lastpage )
        $echo .= '<li><a href="'.$lastpage.'">'.__( 'Última', APP_TEXTDOMAIN ).'</a></li>';
    */
    if ( isset( $echo ) )
        echo $args['before_output'] . $echo . $args['after_output'];
}

function get_term_ancestors( $term )
{
    $ancestors = array_reverse( get_ancestors( $term->term_id, $term->taxonomy ) );
    foreach( $ancestors as $ancestor ) {
        echo '<li class="breadcrumb__item"><a class="underline-effect" href="' . get_term_link( $ancestor ) . '">' . get_term_by( 'id', $ancestor, $term->taxonomy )->name . '</a></li>';
    }
}

function get_nwp_breadcrumb()
{
    global $wp_query;
    global $post;
    if ( !is_front_page() ) {
        echo '<section class="breadcrumb">';
        echo '<div class="container">';
        echo '<div class="row">';
        echo '<div class="col-12">';
        echo '<ul class="breadcrumb__list">';
        $home_breadcrumb = __( 'Home', APP_TEXTDOMAIN );
        $blog_breadcrumb = __( 'Blog', APP_TEXTDOMAIN );
        echo '<li class="breadcrumb__item"><a class="underline-effect" href="' . home_url() . '">' . $home_breadcrumb . '</a></li>';
        switch( true ) {
            case is_page():
                $ancestors = array_reverse( get_ancestors( $post->ID, 'page' ) );
                foreach( $ancestors as $ancestor ) {
                    echo '<li class="breadcrumb__item"><a class="underline-effect" href="' . get_permalink( $ancestor ) . '">' . get_post( $ancestor )->post_title . '</a></li>';
                }
                echo '<li class="breadcrumb__item">' . $post->post_title . '</li>';
                break;
            case is_home():
                echo '<li class="breadcrumb__item">' . $blog_breadcrumb . '</li>';
                break;
            case is_category():
            case is_tag():
                $term = $wp_query->queried_object;
                get_term_ancestors( $term );
                echo '<li class="breadcrumb__item">' . $term->name . '</li>';
                break;
            case is_single():
                $term = get_the_terms( $post->ID, 'category' );
                if ( $term ) {
                    $term = $term[0];
                    get_term_ancestors( $term );
                    echo '<li class="breadcrumb__item"><a class="underline-effect" href="' . get_term_link( $term->term_id ) . '">' . $term->name . '</a></li>';
                }
                echo '<li class="breadcrumb__item">' . $post->post_title . '</li>';
                break;
        }
        echo '</ul>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</section>';
    }
}

function my_body_classes( $classes ) 
{
    return $classes;
}
add_filter( 'body_class', 'my_body_classes', 10, 1 );

function my_upload_mimes( $mimes = array() )
{
    $mimes['svg'] = 'image/svg';
    $mimes['png'] = 'image/png';
    $mimes['gif'] = 'image/gif';
    return $mimes;
}
add_filter( 'upload_mimes', 'my_upload_mimes', 1, 10 );

function custom_excerpt_length( $length )
{
    return $excerpt_length = 35;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more( $more )
{
    return '...';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

function login_page_css()
{?>
    <style type="text/css">
        body.login,
        .wp-auth-check {
            background: #294459;
        }
        body.login #backtoblog a, body.login #nav a {
            color: #fff;
        }
        body.login #login h1 a, .login h1 a {
            height: 130px !important;
            background-image: url("<?php echo get_stylesheet_directory_uri(); ?>/public/assets/images/login-logo-white.svg");
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'login_page_css' );

