<?php $block = get_query_var( 'block' ); 
if ( !empty( $block ) ) : ?>
    <div class="acf-block-component acf-block-body">
        <div>
            <div class="acf-block-fields acf-fields">
                <div class="acf-field acf-field-message">
                    <div class="acf-label">
                        <h4><strong><?php echo $block['title']; ?></strong></h4>
                    </div>
                    <div class="acf-input">
                        <p><?php echo $block['description']; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;