<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nwp_theme
 */
?>
<?php echo ( $option = get_option('my_404_code') ) ? $option : sprintf( __( 'Parece que te has perdido en la densidad%1$sNo te preocupes, te regresaremos al inicio%2$s', APP_TEXTDOMAIN ), '<span>', '</span>' ) ;  ?>