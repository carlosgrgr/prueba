<?php
/**
 * Template part for displaying header nav
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nwp_theme
 */

$translations = \Nwp\MultilingualPress\NwpMultilingualPress::get_translations();
if ( isset( $translations[ \get_current_blog_id() ] ) ) {
    $current_language =  $translations[\get_current_blog_id()];
    unset( $translations[\get_current_blog_id()] );
}
?>
<header>
</header>
<?php get_nwp_breadcrumb();
