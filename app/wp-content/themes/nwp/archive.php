<?php
/**
* The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nwp_theme
 */
get_header();

get_footer();
