<?php
/**
* The template for displaying blog page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nwp_theme
 */
get_header();
get_template_part( 'src/Posts/views/archive', 'post' );
get_footer(); ?>